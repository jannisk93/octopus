---
title: "MacOS installation"
#series: "Manual"
weight: 5
---

### MacOS Sonoma 14.3

Here are the instructions to build Octopus on MacOS, adding Mac specific details to the {{<manual "Install" "general instructions">}}.
This guide assumes you are using the latest MacOS version, i.e. Sonoma 14.3.
**NOTE WELL**: Building Octopus on MacOS is not fully supported, so even if you follow this instruction thoroughly, the installation might fail.
Even if the installation is successful, it is still likely that many tests in 'make check' will fail.

This guide was tested on an Apple M1 Pro chip, but it should be compatible with an Inter processor. However, you should check the installation folder for Homebrew.

#### Before Installing

First of all, you need to install Xcode Command Line Tools. This provides Apple's developer tools, including compilers. This is required to install the package manager required to install Octopus. To install Xcode Command Line Tools, open the Terminal app and execute {{<code "xcode-select --install">}}

Find out the architecture of your CPU: To do that it’s enough to click on the Apple logo in the top left corner and select the first item in the drop-down (About this Mac…). Then look at the information for the chip, if it says Apple M1/M1 Pro/M2 then the architecture is arm64, otherwise, it’s x86_64 (Intel CPU)

#### Choose the package manager that you want to use

It is possible to install Octopus with the two most know package managers for MacOS, [HomeBrew](https://brew.sh/) and [MacPorts](https://www.macports.org/index.php).  <br>
Once you chose it, please follow the instructions on the package manager's website to correctly install it on your machine.  <br>
Finally, close and reopen your terminal to make changes effective.  <br>

#### Choose the installation mode, manual or automatic

Irrespective of the package manager that you chose, you can either perform a manual or an automatic installation of Octopus.
- Manual installation: it is meant that you manually run the commands (in the terminal) to install the compilers and the libraries required to run Octopus. This gives you the freedom to control everything, libraries to install, installation flags, ... Choose this option if you know what you are doing
- Automatic installation: for both Homebrew and Macports, we provide an installation script which takes care of everything, from installing the libraries to compiling Octopus

#### Get Octopus

After choosing the package manager and installing it, and after choosing your preferred installation method, it is time to get Octopus. <br>
Create a folder in your favorite location and open a Terminal window in that folder. To do this, type `cd ` then drag and drop the folder from the Finder to the Terminal. <br>

##### Get the source code

**GIT CLONE:** To use git clone you will need to have `git` installed on your local computer and to have a GitLab account. The official repository page is [Octopus-code](https://gitlab.com/octopus-code/octopus). Then execute in the terminal:
```bash
git clone https://gitlab.com/octopus-code/octopus.git
```

**DOWNLOAD SOURCE:** Go to the {{<versioned-link "releases" "releases">}} and download the latest version.
Place the downloaded tar.gz file and place it in the folder you created. Then extract all files by double clicking or using the terminal command `tar xzf octopus-{{<octopus-version>}}.tar.gz`

#### Automatic installation

In this section the automatic installation mode will be described. You may skip this section if you chose to install manually. <br>
The scripts are located in the Octopus repository, as
- Homebrew: `<path_to_octopus>/scripts/build/build_octopus_macos_brew.sh`
- MacPorts: `<path_to_octopus>/scripts/build/build_octopus_macos_macports.sh` (note that this is currently untested and unsupported)

Finally, **remember to check on Homebrew website for the compatibility of the packages with your architecture** 
To do it, got to https://formulae.brew.sh/ and type the formula you want to install (e.g. libxc).

It is possible to install Octopus with the two most known package managers for MacOS, [HomeBrew](https://brew.sh/) and [MacPorts](https://www.macports.org/index.php).  <br>
Once you chose it, please follow the instructions on the package manager's website to correctly install it on your machine.  <br>
Finally, close and reopen your terminal to make changes effective.  <br>

Depending on what package manager you installed installed, Homebrew or MacPorts, use the corresponding shell script. If you installed both of them, please only run **one** of the two scripts. They can be run with the following command from the terminal:
```bash
./build_octopus_macos_brew.sh
./build_octopus_macos_macports.sh
```
Both offer some installation options, which may be specified after the command above
- `--prefix=`: Specify the installation path for Octopus binaries
  - If this option is not specified, the script will create a folder `installed` in the folder where the script is located. Octopus will be installed there (`<...>/installed/bin/octopus`)
- `--root-path=`: Specify the path to Octopus repository
  - About the repository path: If the value given by the user is incorrect, the script will fail. If this option is not specified, 
  the script will assume that the repository folder is **the parent of the current working folder**. Also, please note that that the symlink is not navigated, so if you are running from a symlink of `octopus/scripts/build`, this script will fail. Make sure you copy this script in the desired folder (see step below), or you correctly specify both `--prefix` and `--root-path`.
- `-l` or `--install-libs`: If present, the script will install (or reinstall) the required libraries. A library is a collection of code
which provide tools to optimize the code or to solve some problems. For instance, a required library for Octopus is
[libxc](https://libxc.gitlab.io/) which provides the exchange-correlation functionals to solve DFT.
  - **WARNING**: You may have some of the libraries and/or the compilers already installed in your computer. It might be that some of those installations are corrupted, or they are not suitable for building Octopus. For example, if OpenMPI was compiled using Apple's DevTools compiler, you are likely to have error at configuring or at linking stage. Examples of such errors are _"C compiler does not work"_ or that it is not possible to link non-portable libraries.
  If such a thing happen, you should manually uninstall the libraries and the compilers that you installed using the package manager (to do that, please refer to the website of the package manager that you use, [HomeBrew](https://brew.sh/) and [MacPorts](https://www.macports.org/index.php)). Afterwards, run the Octopus build script with the `-l` flag.
- `-s` or `--skip-config`: If present, Octopus will not be configured, but only built (`make install`). To use this, you must have successfully configured Octopus in the past
- `-d` or `--debug`: In present, Octopus will be installed with debug flags. Especially recommended for developers.

For example, if you want to install Octopus with Homebrew in your favourite folder, with debug flags and requiring the installation of libraries you should run:
```bash
./build_octopus_macos_brew.sh --prefix=<path_to_favourite_folder> -l -d
```

After understanding the installation options, follow these steps:
1. Open the terminal where you want to install Octopus. Create a folder that will contain the result of the build: 
```bash
mkdir <folder_name>
cd <folder_name>
```
2. Copy the installation script here
```bash
cp <path_to_octopus>/scripts/build/build_octopus_macos_brew.sh .
```
3. Change the terminal type to `bash` using:
```bash
exec bash
```
4. Run the script using the commands below. Then enjoy a long coffee!
```bash
./build_octopus_macos_brew.sh --root-path=<path_to_octopus> <options>
```

#### Manual installation

In this section the manual installation of Octopus will be described. Please be aware that Octopus has many dependencies, thus it is likely that you will encounter errors at some stage of the process. <br>

1. Configuring the workspace
  - Open a terminal. First, you have to make sure that your `PATH` environmetal variable is correct. This defines where your computer looks for executables, libraries... You can check it by running `echo $PATH`. The correct value depends on the package manager that you chose:
    - MacPorts: `PATH=/opt/local/bin:/opt/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:<other_paths>`
    - Homebrew: Since the installation path of Homebrew depends on the CPU architecture, the `PATH` should vary accordingly:
      - ARM: `PATH=/opt/homebrew/bin:/opt/homebrew/sbin:/usr/bin:/usr/sbin:/bin:/sbin:<other_paths>`
      - Intel: `PATH=/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:<other_paths>`
  You can modify the path by using
```bash
export PATH=<needed_path>:$PATH
```
2. Install libraries (skip this step if you already have them installed)
  - Remember to check on your package manager's website that all required packages are compatible with your operating system
  - Install compilers:
    - LLVM - Low Level Virtual Machine.
      - MacPorts: `sudo port install llvm-16`
      - Homebrew: `brew install llvm`
        - Before configuring Octopus you will need to run `export LDFLAGS="-Xlinker -L$homebrew/opt/llvm/lib -rpath=$homebrew/opt/llvm/lib/c++"`, where `$homebrew` is the Homebrew prefix in your computer
    - GCC - C, C++, Fortran compiler
      - MacPorts: `sudo port install gcc13`
      - Homebrew: `brew install gcc`
    - OpenMPI - Install this only if you wish Octopus to run in parallel
      - MacPorts: `sudo port install openmpi-gcc13 +fortran`
      - Homebrew: `brew install --cc=gcc-13 open-mpi`
    - Build tools
      - MacPorts: 
        ```bash
        sudo port install autoconf
        sudo port install automake
        sudo port install libtool
        ```
      - Homebrew:
        ```bash
        brew install autoconf
        brew install automake
        brew install libtool
        ```
  - Install required libraries:
    - LibXC - This library contains the exchange and correlations functionals. Please note that Octopus 12.x **does not** support `libxc6`. The support will be added in the next releases (starting from Octopus 13).
      - MacPorts: `sudo port install libxc5 +fortran`
      - Homebrew: `brew install libxc`
    - Lapack and BLAS - These are numerical libraries for linear algebra (Linear Algebra PACKage and Basic Linear Algebra Subprograms)
      - MacPorts: `sudo port install lapack +gcc13`
      - Homebrew: `brew install lapack`
    - GSL - GNU Scientific Library
      - MacPorts: `sudo port install gsl`
      - Homebrew: `brew install --cc=gcc-13 gsl`
        - Note: please do not omit `--cc=gcc-13`. This is needed to correcly compile the library, otherwise the parsing of some functions
        in the input file (e.g. `sqrt(n)`) might fail.
    - FFTW - This is a library for computing discrete Fourier transform
      - MacPorts: `sudo port install fftw-3 +openmpi`
        - Note: add the `+openmpi` flag only if you installed OpenMPI
      - Homebrew: `brew install fftw`
  - Install other libraries
    - ScaLapack and BLACS - These are numerical libraries for parallel linear algebra operations (SCAlable Linear Algebra PACKage and Basic Linear Algebra Communication Subprograms). These library are highly recommended to run Octopus in parallel. Install them only if you installed OpenMPI
      - MacPorts: `sudo port install scalapack +openmpi`
      - Homebrew: `brew install scalapack`
    - CGAL - [Computational Geometry Algorithms Library](https://www.cgal.org/) is a library that provides high-efficiency geometric algorithms
      - MacPorts: `sudo port install cgal5`
      - Homebrew: `brew install cgal`
    - NETCDF - [Network Common Data Form](https://www.unidata.ucar.edu/software/netcdf/) is a library that allows creation, access
    and sharing of data. This format is machine independent and has interfaces with many programming languages.
      - MacPorts: `sudo port install netcdf`
      - Homebrew: `brew install netcdf-fortran`
3. Create the configure script and run it. To do that, go to the Octopus root folder and run
```bash
autoreconf -i
```
Then run it with
```bash
./configure <configure options>
```
You can find the configure options by running `configure --help`
Here are some common issues that can happen:
  - `C compiler does not work`: in this case the configure script detected that the `C` compiler cannot create executables. We suggest to create a simple `C` program and compile it. If that work, check that the paths were set correctly when you installed the compilers (especially if you are using an mpi compiler). You can look at the `config.log`.
  - `Cannot find header mpi.h`: this can only happen if you are installing Octopus with mpi support. Check that your mpi compilers are corretly working (perhaps by compiling a simple program). Check that your linking flags are correct. If the issue persists, consider reinstalling your compilers (this will clean your environment).
  - `Cannot find library`: if the configure script stops because it cannot find a library, it means that that library is required (e.g. `libxc`). Please check that the provided prefix is correct and that your installation is not corrupted.
  - Remember to always make sure that the `PATH` is set correctly (see point 1)
4. Run `make` and `make install` to build Octopus


Congratulations! You now have Octopus running on your computer <br>
Go to the [tutorials](https://www.octopus-code.org/documentation/main/tutorial/) to start learning how to use it.
