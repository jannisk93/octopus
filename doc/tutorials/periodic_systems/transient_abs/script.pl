#!/usr/bin/env perl

$delay = 0;
$delaymax = 1500;
$incr = 50;

`rm -r td.general_delay*`;

do{
  print("TD propagation for a delay of $delay");
  `cp inp_ref inp`;
  $sed_param1="s/DELAY/${delay}/g";
  `sed -i "$sed_param1" inp`;
  `mpirun -n 4 octopus > log`;
  `oct-conductivity > log`;
  `mv td.general td.general_delay${delay}`;

  $delay=$delay+$incr;
}while($delay <= $delaymax);
