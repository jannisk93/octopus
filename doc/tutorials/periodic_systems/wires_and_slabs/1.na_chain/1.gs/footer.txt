             Info: Writing states. 2023/06/07 at 15:09:45


        Info: Finished writing states. 2023/06/07 at 15:09:45

Info: SCF converged in   15 iterations

Info: Number of matrix-vector products:        869
Info: Finished writing information to 'restart/gs'.

             Calculation ended on 2023/06/07 at 15:09:45

                          Walltime:  03.233s

Octopus emitted 3 warnings.

Octopus used 1 experimental feature:

  Since you used one or more experimental features, results are likely
  wrong and should not  be considered as valid scientific data.  Check

  https://www.octopus-code.org/documentation/main/variables/execution/debug/experimentalfeatures

  or contact the octopus developers for details.

