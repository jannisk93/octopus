set xlabel "x (a.u.)"
set ylabel "wavefunction"
set t postscript enhanced color font "Monospace-Bold,25" landscape size 11,8.5
set output "QOCT_wavefunctions.eps"
set key bottom left

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5


plot [-6:6][] \
    'opt-control/initial/wf-st00001.y=0,z=0' u 1:2 t 'initial' w l lw 2 linecolor rgb "red", \
    'opt-control/target/wf-st00001.y=0,z=0' u 1:2 t 'target' w l lw 2 linecolor rgb "green", \
    'opt-control/final/wf-st00001.y=0,z=0' u 1:2 t 'final, Re' w l lw 2 linecolor rgb "blue",\
    'opt-control/final/wf-st00001.y=0,z=0' u 1:3 t 'final, Im' w l dt 2 lw 2 linecolor rgb "blue",\

