#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps
mpirun -n 8 octopus > log

$HELPER_DIR/extract.sh log "Optimal control iteration #   12" > iter_12.txt
$HELPER_DIR/extract.sh log "Final propagation with the best field"  > final.txt

gnuplot plot2.gp
gnuplot plot3.gp

cp tutorial.sh *.gp  *.eps *.txt $OCTOPUS_TOP/doc/tutorials/other/basic_qoct/2.qoct/
