#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps
mpirun -n 8 octopus 

gnuplot plot.gp


cp tutorial.sh plot.gp *.eps $OCTOPUS_TOP/doc/tutorials/other/basic_qoct/1.gs/
