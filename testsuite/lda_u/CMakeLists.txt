foreach (test IN ITEMS
		01-nio
		02-ACBN0
		03-ACBN0_restricted
		04-ACBN0_isolated
		05-forces
		06-laser
		07-noncollinear
		08-loewdin
		09-basis_from_states
		10-intersite
		11-full_delta
		12-intersite_spinors
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
