foreach (test IN ITEMS
		01-free-propagation
		02-external-current
		03-linear-medium
		04-linear-medium-from-file
		05-plane_waves
		06-circular-polarization
		07-mode-resolved-maxwell-ks-propagation
		08-restart-maxwell
		09-drude-medium-from-file
		10-current-to-maxwell
		11-leapfrog
		12-tddft-currents-to-maxwell
		13-extsource-bessel
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
