# 1D harmonic oscillator 

FromScratch = yes
CalculationMode = gs

Dimensions = 1

# for 1 electron, not use any functional for e-e interaction
# one should turn this off when studying many electrons
XCFunctional = none

# for 1 electron, not include the Coulomb interaction
# one should comment this out when studying many electrons
XCPhotonIncludeHartree = no

MaximumIter = 2000

Eigensolver = rmmdiis

MixField = density

# mixing strategy, which depends on which system one study
Mixing = 0.1
MixingResidual = 0.0001
MixingRestart = 1

ConvRelDens = 1e-14
EigenSolverTolerance = 1e-26

Radius = 10.50
Spacing = 0.07

PseudopotentialSet = none

%Species
  "SHO" | species_user_defined | potential_formula | "x^2/2" | valence | 1
%

%Coordinates
  "SHO" | 0
%


ExperimentalFeatures = yes

# to turn on the QEDFT px functional
XCPhotonFunctional = photon_xc_wfn

# PhotonXCEnergyMethod = 1: pxLDA (exchange virial relation)
# PhotonXCEnergyMethod = 2: px for one electron (do not use for many electrons)
PhotonXCEnergyMethod = virial

# this is the eta_{c} factor introduced in our manuscript
PhotonXCEtaC = 0.75

#
%PhotonModes
5.1 | 4.000 | 1
%
