foreach (test IN ITEMS
		01-free_electrons
		02-cosine_potential
		03-sodium_chain
		04-silicon
		05-lithium
		06-h2o_pol_lr
		07-mgga
		08-benzene_supercell
		09-etsf_io
		10-berkeleygw
		11-silicon_force
		12-boron_nitride
		13-primitive
		14-silicon_shifts
		15-bandstructure
		16-sodium_chain_cylinder
		17-aluminium
		18-TiO2
		19-unfolding
		20-masked_periodic_boundaries
		21-magnon
		22-berry
		23-hybrids
		24-hartree_fock_1D
		25-Fe_polarized
		26-Na
		27-Ar
		28-mgga_kli
		29-soc_solids
		30-stress
		32-photodoping
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
