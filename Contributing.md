# Contributing

Octopus is an open source scientific code and contributions from users are welcome. The basics of contributing are
documented below. For more details, please consult the [official website page](https://octopus-code.org/documentation/main/developers/).
Octopus developers can be reached via the [Octopus Gitlab](https://gitlab.com/octopus-code/octopus), and via the 
developers' [mailing list](octopus-devel-bounces@lists.octopus-code.org).

## Table of contents

1. [Opening Issues](#opening-issues)
   - [Bug Report](#bug-report)
   - [Reporting Flakey Tests](#reporting-flakey-tests)
   - [Feature Suggestion](#feature-suggestion)
2. [Opening Merge Requests](#opening-merge-requests)
   - [Adding Modules to CMakeLists.txt](#adding-modules-to-cmakeliststxt)

## Opening Issues

Issues can be opened on Octopus's Gitlab [Issues](https://gitlab.com/octopus-code/octopus/-/issues) page and typically
fall into one of two categories: bug report or feature requestion.

### Bug Report

If an issue relates to a bug, it's important to provide some essential information such that the Octopus team is 
able to reproduce the problem:

* OS type or HPC system
* Compiler version
* Configure settings (autotools or cmake)
* Octopus input file
* Standard out/error
* Any relevant output files

Give a complete description of the problem experienced. An Octopus developer will triage the issue and address 
accordingly.

### Reporting Flakey Tests

Octopus has a small proportion of tests that will fail on a given platform, or with a given toolchain, due to numerical
fluctuations that cause results to slightly exceed the test tolerances. If you experience a flakey test, please report
it in the table provided in Gitlab [issue 966](https://gitlab.com/octopus-code/octopus/-/issues/966).

### Feature Suggestion

Octopus also welcomes feature suggestions. In this instance, please provide some motivation for the feature and any 
background theory required to implement the feature. Note that it may not always be practical for the Octopus team to 
implement a feature if it is large, or requires serious refactoring beforehand. Where possible, users are encouraged to 
contribute their own features via merge requests (MRs).

## Opening Merge Requests

Octopus developers are happy to assist with all aspects of merging code. To aid in the review process, there are 
some important, fundamental points to consider. Merge requests should:

* Be branched off the default Gitlab branch, `main` (please avoid `develop`!)
* Have a clear description of the feature or bug fix
* Only address one conceptual feature or bug (else consider splitting into multiple MRs)
* All subroutines and functions must be documented with doxygen
* No commented-out or dead code should be included
* Include a regression test that demonstrates the new feature or bug fix
* If a new module is added, it should be included in both the autotools and [cmake](#adding-modules-to-cmakeliststxt) 
  builds systems, whilst both build systems are supported (Octopus 15 and 16)
* If the feature introduces new physics, a tutorial should also be included (this can be discussed in the MR)

### Adding Modules to CMakeLists.txt

A `CMakeLists.txt` file is defined in each subfolder of the `src/`. In order to add a new fortran source file to
the build, simply append the `target_sources` arguments with the file name:

```cmake
target_sources(Octopus_lib PRIVATE
		module.F90
		...
)
```

This specifies sources to use when building a target `Octopus_lib`.
