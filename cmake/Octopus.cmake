include_guard(GLOBAL)

include(FindPackageHandleStandardArgs)
include(CMakeFindDependencyMacro)
find_package(PkgConfig REQUIRED)

# TODO: Remove when cmake_minimum_required >= 3.24
macro(Octopus_FetchContent_Declare name)
    #[===[.md
    # Octopus_FetchContent_Declare

    A short compatibility function to mimic FIND_PACKAGE_ARGS functionality. All arguments are equivalent to
    upstream `FetchContent_Declare` from version
    [`3.24`](https://cmake.org/cmake/help/v3.24/module/FetchContent.html#command:fetchcontent_declare)

    This function also defines `LIST_VAR`
    ]===]

    list(APPEND CMAKE_MESSAGE_CONTEXT Octopus_FetchContent_Declare)
    set(ARGS_Options "OVERRIDE_FIND_PACKAGE")
    set(ARGS_OneValue "LIST_VAR")
    set(ARGS_MultiValue "FIND_PACKAGE_ARGS")
    cmake_parse_arguments(OFCD_ARGS "${ARGS_Options}" "${ARGS_OneValue}" "${ARGS_MultiValue}" ${ARGN})

    if (NOT DEFINED OFCD_ARGS_LIST_VAR)
        # Default to octopus variable
        set(OFCD_ARGS_LIST_VAR Octopus_ext_libs)
    endif ()

    # Note: Defining as macro due to find_package limitations
    if (CMAKE_VERSION VERSION_GREATER_EQUAL 3.24)
        # If cmake supports `FIND_PACKAGE_ARGS` simply pass the arguments to the native function
        FetchContent_Declare(${name}
                ${ARGN})
    else ()
        set(${name}_FOUND FALSE)
        if (OFCD_ARGS_OVERRIDE_FIND_PACKAGE)
            message(FATAL_ERROR "Cannot back-port OVERRIDE_FIND_PACKAGE")
        endif ()

        # First check for FETCHCONTENT_SOURCE_DIR_<uppercaseName>
        # this always takes precedence, and should be treated as a FetchContent package
        string(TOUPPER ${name} upper_name)
        if (NOT DEFINED FETCHCONTENT_SOURCE_DIR_${upper_name})
            # Next handle according to FETCHCONTENT_TRY_FIND_PACKAGE_MODE
            if (NOT DEFINED FETCHCONTENT_TRY_FIND_PACKAGE_MODE)
                set(FETCHCONTENT_TRY_FIND_PACKAGE_MODE OPT_IN)
            endif ()
            # Check if `FIND_PACKAGE_ARGS` was passed
            if ((FETCHCONTENT_TRY_FIND_PACKAGE_MODE STREQUAL "OPT_IN" AND DEFINED OFCD_ARGS_FIND_PACKAGE_ARGS) OR
            (FETCHCONTENT_TRY_FIND_PACKAGE_MODE STREQUAL "ALWAYS"))
                # Try to do find_package. If it fails fallthrough and use FetchContent
                # The package should have `FIND_PACKAGE_ARGS REQUIRED` to deny fallthrough
                find_package(${name} ${OFCD_ARGS_FIND_PACKAGE_ARGS})
                # Note: This is a macro so we cannot use early return()
            endif ()
            # The remaining case is `FETCHCONTENT_TRY_FIND_PACKAGE_MODE == NEVER` which should should fall through
        endif ()
        # Continue to call `FetchContent_Declare` as usual if find_package was called and it did not succeed
        if (NOT ${name}_FOUND)
            # Pass all other arguments that were used
            FetchContent_Declare(${name}
                    ${OFCD_ARGS_UNPARSED_ARGUMENTS})
        endif ()
    endif ()

    # Finally add to LIST_VAR argument to be handled by FetchContent_MakeAvailable
    # Note: We should always add it to the LIST_VAR, but if we are in the compatibility mode, skip adding it to the list
    if (CMAKE_VERSION VERSION_LESS 3.24 AND ${name}_FOUND)
        # We are in the compatibility branch and the find_package has already succeeded
        # (do nothing)
    else ()
        list(APPEND ${OFCD_ARGS_LIST_VAR} ${name})
    endif ()

    # Sanitize local variables in order to not contaminate future calls
    set(ARGS_Options)
    set(ARGS_OneValue)
    set(ARGS_MultiValue)
    set(OFCD_ARGS_UNPARSED_ARGUMENTS)
    set(OFCD_ARGS_OVERRIDE_FIND_PACKAGE)
    set(OFCD_ARGS_LIST_VAR)
    set(OFCD_ARGS_FIND_PACKAGE_ARGS)
    list(POP_BACK CMAKE_MESSAGE_CONTEXT)
endmacro()

macro(Octopus_FindPackage name)
    #[===[.md
    # Octopus_FindPackage

    A compatibility macro that links `find_package(CONFIG)` packages with `pkg-config`. This should only
    be called within the `Find<PackageName>.cmake` file.

    Note: Version range syntax is not supported for pkg-config searching. Only the lower bound will be respected.

    ]===]

    list(APPEND CMAKE_MESSAGE_CONTEXT "Octopus_FindPackage")
    set(ARGS_Options "HAVE_FALLBACK")
    set(ARGS_OneValue "")
    set(ARGS_MultiValue "NAMES;VERSIONS;PKG_MODULE_NAMES;PKG_MODULE_SPECS")
    cmake_parse_arguments(OFP_ARGS "${ARGS_Options}" "${ARGS_OneValue}" "${ARGS_MultiValue}" ${ARGN})

    # First try to find native <PackageName>Config.cmake
    # Build the arguments
    # COMPONENTS
    set(_comp_args)
    set(_opt_comp_args)
    if (DEFINED ${name}_FIND_COMPONENTS)
        list(APPEND _comp_args COMPONENTS)
        foreach (_comp IN LISTS ${name}_FIND_COMPONENTS)
            if (${name}_FIND_REQUIRED_${_comp})
                list(APPEND _comp_args ${_comp})
            else ()
                if (NOT DEFINED _opt_comp_args)
                    list(APPEND _opt_comp_args OPTIONAL_COMPONENTS)
                endif ()
                list(APPEND _opt_comp_args ${_comp})
            endif ()
        endforeach ()
    endif ()

    # Version
    # Try range format first, otherwise use the default
    set(_version_args ${${name}_FIND_VERSION_RANGE})
    if (NOT DEFINED _version_args)
        set(_version_args ${${name}_FIND_VERSION})
    endif ()
    if (${name}_FIND_VERSION_EXACT)
        list(APPEND _version_args EXACT)
    endif ()

    # QUIET
    set(_quiet_arg)
    if (${name}_FIND_QUIETLY)
        list(APPEND _quiet_arg QUIET)
    endif ()

    # REQUIRED
    set(_required_arg)
    if (NOT OFP_ARGS_HAVE_FALLBACK AND ${name}_FIND_REQUIRED)
        list(APPEND _required_arg REQUIRED)
    endif ()

    # REGISTRY_VIEW
    set(_registry_view_arg)
    if (${name}_FIND_REGISTRY_VIEW)
        list(APPEND _registry_view REGISTRY_VIEW ${${name}_FIND_REGISTRY_VIEW})
    endif ()

    # NAMES
    set(_names_args)
    if (DEFINED OFP_ARGS_NAMES)
        list(APPEND _names_args NAMES ${OFP_ARGS_NAMES})
    endif ()

    # Disable CMAKE_REQUIRE_FIND_PACKAGE_<PackageName> because there is a fallthrough to pkg-config
    set(CMAKE_REQUIRE_FIND_PACKAGE_${name} FALSE)

    # Try <PackageName>Config.cmake
    if (OFP_ARGS_VERSIONS)
        # If VERSIONS argument is passed, use that instead of `_version_args`
        foreach (_try_version IN LISTS OFP_ARGS_VERSIONS)
            find_package(${name} ${_try_version} ${_quiet_arg} CONFIG
                    ${_comp_args}
                    ${_opt_comp_args}
                    ${_registry_view_arg}
                    ${_names_args}
            )
            if (${name}_FOUND)
                break()
            endif ()
        endforeach ()
    else ()
        # Otherwise use whatever was provided in the parent `find_package(MODULE)`
        find_package(${name} ${_version_args} ${_quiet_arg} CONFIG
                ${_comp_args}
                ${_opt_comp_args}
                ${_registry_view_arg}
                ${_names_args}
        )
    endif ()
    if (${name}_FOUND)
		find_package_handle_standard_args(${name}
				CONFIG_MODE HANDLE_COMPONENTS
		)
    else ()
        # Try pkg-config next
        # Construct the moduleSpec to search for
        if (NOT DEFINED OFP_ARGS_PKG_MODULE_SPECS)
            if (NOT DEFINED OFP_ARGS_PKG_MODULE_NAMES)
                set(OFP_ARGS_PKG_MODULE_NAMES ${name})
            endif ()
            if (DEFINED ${name}_FIND_VERSION_RANGE)
                # Can only parse the minimum requirement
                foreach (_pkg_name IN LISTS OFP_ARGS_PKG_MODULE_NAMES)
                    list(APPEND OFP_ARGS_PKG_MODULE_SPECS "${_pkg_name}>=${${name}_FIND_VERSION_MIN}")
                endforeach ()
            elseif ({${name}_FIND_VERSION_EXACT)
                # Requesting exact version
                foreach (_pkg_name IN LISTS OFP_ARGS_PKG_MODULE_NAMES)
                    list(APPEND OFP_ARGS_PKG_MODULE_SPECS "${_pkg_name}=${${name}_FIND_VERSION}")
                endforeach ()
            elseif (DEFINED ${name}_FIND_VERSION)
                # Otherwise treat the request as minimum requirement
                foreach (_pkg_name IN LISTS OFP_ARGS_PKG_MODULE_NAMES)
                    list(APPEND OFP_ARGS_PKG_MODULE_SPECS "${_pkg_name}>=${${name}_FIND_VERSION}")
                endforeach ()
            else ()
                # Fallthrough if no version is required
                foreach (_pkg_name IN LISTS OFP_ARGS_PKG_MODULE_NAMES)
                    list(APPEND OFP_ARGS_PKG_MODULE_SPECS "${_pkg_name}")
                endforeach ()
            endif ()
        endif ()
        # Call pkg-config
        if (CMAKE_VERSION VERSION_LESS 3.28)
            # https://gitlab.kitware.com/cmake/cmake/-/issues/25228
            set(ENV{PKG_CONFIG_ALLOW_SYSTEM_CFLAGS} 1)
        endif ()
        if (CMAKE_VERSION VERSION_LESS 3.22)
            # Back-porting
            # https://gitlab.kitware.com/cmake/cmake/-/merge_requests/6345
            set(ENV{PKG_CONFIG_ALLOW_SYSTEM_LIBS} 1)
        endif ()
        pkg_search_module(${name}
                ${_required_arg} ${_quiet_arg}
                IMPORTED_TARGET
                ${OFP_ARGS_PKG_MODULE_SPECS})
        # Mark the package as found by pkg-config
        if (${name}_FOUND)
            set(${name}_PKGCONFIG True)
        endif ()
    endif ()

    # Sanitize local variables in order to not contaminate future calls
    set(ARGS_Options)
    set(ARGS_OneValue)
    set(ARGS_MultiValue)
    set(OFP_ARGS_UNPARSED_ARGUMENTS)
    set(OFP_ARGS_NAMES)
    set(OFP_ARGS_PKG_MODULE_NAMES)
    set(OFP_ARGS_PKG_MODULE_SPECS)
    set(_version_args)
    set(_quiet_arg)
    set(_comp_args)
    set(_opt_comp_args)
    set(_registry_view_arg)
    set(_names_args)
    set(_pkg_name)
    list(POP_BACK CMAKE_MESSAGE_CONTEXT)
endmacro()

macro(Octopus_FetchContent_MakeAvailable)
    #[===[.md
    # Octopus_FetchContent_MakeAvailable

    Calls FetchContent<PackageName>_Before.cmake and FetchContent<PackageName>_After.cmake for each project

    ]===]

    list(APPEND CMAKE_MESSAGE_CONTEXT "Octopus_FetchContent_MakeAvailable")
    set(ARGS_Options "")
    set(ARGS_OneValue "MODULES_PATH;PREFIX;BEFORE_SUFFIX;AFTER_SUFFIX")
    set(ARGS_MultiValue "")
    cmake_parse_arguments(OFCMA_ARGS "${ARGS_Options}" "${ARGS_OneValue}" "${ARGS_MultiValue}" ${ARGN})

    if(NOT DEFINED OFCMA_ARGS_MODULES_PATH)
        set(OFCMA_ARGS_MODULES_PATH ${PROJECT_SOURCE_DIR}/cmake/compat)
    endif ()
    if(NOT DEFINED OFCMA_ARGS_PREFIX)
        set(OFCMA_ARGS_PREFIX FetchContent)
    endif ()
    if(NOT DEFINED OFCMA_ARGS_BEFORE_SUFFIX)
        set(OFCMA_ARGS_BEFORE_SUFFIX _Before)
    endif ()
    if(NOT DEFINED OFCMA_ARGS_AFTER_SUFFIX)
        set(OFCMA_ARGS_AFTER_SUFFIX _After)
    endif ()

    foreach (pkg IN LISTS OFCMA_ARGS_UNPARSED_ARGUMENTS)
        include(${OFCMA_ARGS_MODULES_PATH}/${OFCMA_ARGS_PREFIX}${pkg}${OFCMA_ARGS_BEFORE_SUFFIX}.cmake OPTIONAL)
        FetchContent_MakeAvailable(${pkg})
        include(${OFCMA_ARGS_MODULES_PATH}/${OFCMA_ARGS_PREFIX}${pkg}${OFCMA_ARGS_AFTER_SUFFIX}.cmake OPTIONAL)
    endforeach ()
    set(ARGS_Options)
    set(ARGS_OneValue)
    set(ARGS_MultiValue)
    set(OFCMA_ARGS_UNPARSED_ARGUMENTS)
    set(OFCMA_ARGS_MODULES_PATH)
    set(OFCMA_ARGS_PREFIX)
    set(OFCMA_ARGS_BEFORE_SUFFIX)
    set(OFCMA_ARGS_AFTER_SUFFIX)
    list(POP_BACK CMAKE_MESSAGE_CONTEXT)
endmacro()
