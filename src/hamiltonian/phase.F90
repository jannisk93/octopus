!! Copyright (C) 2009 X. Andrade
!! Copyright (C) 2024 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module phase_oct_m
  use accel_oct_m
  use batch_oct_m
  use batch_ops_oct_m
  use comm_oct_m
  use debug_oct_m
  use derivatives_oct_m
  use distributed_oct_m
  use electron_space_oct_m
  use global_oct_m
  use grid_oct_m
  use hardware_oct_m
  use kpoints_oct_m
  use math_oct_m
  use mesh_oct_m
  use messages_oct_m
  use mpi_oct_m
  use profiling_oct_m
  use space_oct_m
  use states_elec_oct_m
  use states_elec_dim_oct_m
  use submesh_oct_m
  use types_oct_m
  use wfs_elec_oct_m

  implicit none

  private

  public ::                                         &
    phase_t

  !> @brief A container for the phase
  !!
  !! #### the phase of the wave function:
  !!
  !! Octopus stores in memory only the perioc part of the Bloch state.
  !! For some operations, like derivatives , we need the full Bloch state and we need to apply its phase.
  !! This does
  !! \f[ \psi \to \psi e^{\mp i(\mathbf{k}+\mathbf{A})\cdot\mathbf{r}} \f]
  !! where the sign is a minus if conjugate is false, \f$ \mathbf{k} \f$ is the k-point
  !! associated with the block of states \f$\psi\f$, and \f$ \mathbf{A}\f$ is the
  !! dipole part of the vector potential present in the Hamiltonian.
  !!
  !! The phase is updated by Hamiltonian_elec_update.
  !! The wfs_elec_oct_m::wfs_elec_t also stores the fact that the phase
  !! was added (wfs_elec_oct_m::wfs_elec_t::has_phase) and knows
  !! its k-point index (wfs_elec_oct_m::wfs_elec_t::ik),
  !! such that it can finds the phase to apply from the array stored in hamiltonian_elec_t::kpoints.
  !!
  !! The phase must be set (phase_t::apply_phase() with conjugate=false)
  !! for np_part point _after_ applying the boundary conditions
  !! such that the boundary points have the right phase
  !! \f$ e^{- i(\mathbf{k}+\mathbf{A})\cdot(\mathbf{r}+\mathbf{R})} \f$.
  !!
  !! #### The phase correction
  !!
  !! This is an optimisation technique. This is intended to be applied _before_ the boundary conditions.
  !! Then boundary points are copied with the phase, but the code applies the phase correction at the time of copy.
  !! This allows in particular to not set and unset the phase when applying the Hamiltonian several
  !! time to some batch, like in eigensolver and exponential operators.
  !!
  type phase_t
    private
    CMPLX,                   allocatable  :: phase(:, :)       !< phase factor:
    !!                                                            (1:gr%np_part, hm%d%kpt%start:hm%d%kpt%end);
    !!                                                            set in hamiltonian_elec_oct_m::hamiltonian_elec_init()
    CMPLX, public,           allocatable  :: phase_corr(:,:)   !< phase correction:
    !!                                                            (gr%np+1:gr%np_part, hm%d%kpt%start:hm%d%kpt%end);
    !!                                                            set in hamiltonian_elec_oct_m::hamiltonian_elec_init()
    CMPLX,                   allocatable  :: phase_spiral(:,:) !< phase for spiral boundaruy conditions:
    !!                                                            (1:gr%np_part-sp, 1:2);
    !!                                                            set in hamiltonian_elec_oct_m::hamiltonian_elec_init()
    type(accel_mem_t)                      :: buff_phase
    type(accel_mem_t)                      :: buff_phase_spiral
    type(accel_mem_t), public              :: buff_phase_corr
    integer                                :: buff_phase_qn_start
    FLOAT, public,            pointer      :: spin(:,:,:) => null()
  contains
    procedure :: init => phase_init_phases
    !< @copydoc phase_oct_m::phase_init_phases
    procedure :: update => phase_update_phases
    !< @copydoc phase_oct_m::phase_update_phases
    procedure :: end => phase_end
    !< @copydoc phase_oct_m::phase_end
    procedure :: set_phase_corr => phase_set_phase_corr
    !< @copydoc phase_oct_m::phase_set_phase_corr
    procedure :: unset_phase_corr => phase_unset_phase_corr
    !< @copydoc phase_oct_m::phase_unset_phase_corr
    procedure :: apply_to => phase_apply_batch
    !< @copydoc phase_oct_m::phase_apply_batch
    procedure :: apply_to_single => phase_apply_mf
    !< @copydoc phase_oct_m::phase_apply_mf
    procedure :: apply_phase_spiral => phase_phase_spiral
    !< @copydoc phase_oct_m::phase_phase_spiral
    procedure :: is_allocated => phase_is_allocated
  end type phase_t

contains

  ! ---------------------------------------------------------
  !>@brief Initiliaze the phase arrays and copy to GPU the data
  subroutine phase_init_phases(phase, gr, kpt, kpoints, d, space)
    class(phase_t),      intent(inout) :: phase
    type(grid_t),        intent(in)    :: gr
    type(distributed_t), intent(in)    :: kpt
    type(kpoints_t),     intent(in)    :: kpoints
    type(states_elec_dim_t), intent(in)  :: d
    type(space_t),       intent(in)    :: space

    integer :: ip, ik, sp
    integer(int64) :: ip_inner_global
    FLOAT   :: kpoint(space%dim), x_global(space%dim)

    PUSH_SUB(phase_init_phases)

    ! no e^ik phase needed for Gamma-point-only periodic calculations
    ! unless for velocity-gauge for lasers
    if (accel_is_enabled()) then
      phase%buff_phase_qn_start = kpt%start
    end if
    if(kpoints%gamma_only()) then
      POP_SUB(phase_init_phases)
      return
    end if

    SAFE_ALLOCATE(phase%phase(1:gr%np_part, kpt%start:kpt%end))
    SAFE_ALLOCATE(phase%phase_corr(gr%np+1:gr%np_part, kpt%start:kpt%end))
    !$omp parallel private(ip)
    do ik = kpt%start, kpt%end
      !$omp do
      do ip = gr%np + 1, gr%np_part
        phase%phase_corr(ip, ik) = M_ONE
      end do
      !$omp end do nowait
    end do
    !$omp end parallel

    if (gr%der%boundaries%spiralBC) then
      sp = gr%np
      if (gr%parallel_in_domains) sp = gr%np + gr%pv%np_ghost

      ! We decided to allocate the array from 1:np_part-sp as this is less error prone when passing
      ! the array to other routines, or in particular creating a C-style pointer from phase_spiral(1,1).
      ! We will also update phase_corr and possible other similar arrays.

      SAFE_ALLOCATE(phase%phase_spiral(1:gr%np_part-sp, 1:2))

      ! loop over boundary points
      do ip = sp + 1, gr%np_part
        ! get corresponding inner point
        ip_inner_global = mesh_periodic_point(gr, space, ip)
        x_global = mesh_x_global(gr, ip_inner_global)
        phase%phase_spiral(ip-sp, 1) = &
          exp(M_zI * sum((gr%x(ip, 1:space%dim)-x_global(1:space%dim)) * gr%der%boundaries%spiral_q(1:space%dim)))
        phase%phase_spiral(ip-sp, 2) = &
          exp(-M_zI * sum((gr%x(ip, 1:space%dim)-x_global(1:space%dim)) * gr%der%boundaries%spiral_q(1:space%dim)))
      end do

      if (accel_is_enabled()) then
        call accel_create_buffer(phase%buff_phase_spiral, ACCEL_MEM_READ_ONLY, TYPE_CMPLX, (gr%np_part-sp)*2)
        call accel_write_buffer(phase%buff_phase_spiral, (gr%np_part-sp)*2, phase%phase_spiral)
      end if
    end if


    kpoint(1:space%dim) = M_ZERO

    sp = gr%np
    if (gr%parallel_in_domains) sp = gr%np + gr%pv%np_ghost

    do ik = kpt%start, kpt%end
      kpoint(1:space%dim) = kpoints%get_point(d%get_kpoint_index(ik))
      !$omp parallel private(ip, ip_inner_global, x_global)
      !$omp do
      do ip = 1, gr%np_part
        phase%phase(ip, ik) = exp(-M_zI * sum(gr%x(ip, 1:space%dim) * kpoint(1:space%dim)))
      end do
      !$omp end do

      ! loop over boundary points
      !$omp do
      do ip = sp + 1, gr%np_part
        ! get corresponding inner point
        ip_inner_global = mesh_periodic_point(gr, space, ip)

        ! compute phase correction from global coordinate (opposite sign!)
        x_global = mesh_x_global(gr, ip_inner_global)
        phase%phase_corr(ip, ik) = phase%phase(ip, ik)* &
          exp(M_zI * sum(x_global(1:space%dim) * kpoint(1:space%dim)))
      end do
      !$omp end do nowait
      !$omp end parallel
    end do

    if (accel_is_enabled()) then
      call accel_create_buffer(phase%buff_phase, ACCEL_MEM_READ_ONLY, TYPE_CMPLX, gr%np_part*kpt%nlocal)
      call accel_write_buffer(phase%buff_phase, gr%np_part*kpt%nlocal, phase%phase)
      call accel_create_buffer(phase%buff_phase_corr, ACCEL_MEM_READ_ONLY, TYPE_CMPLX, (gr%np_part - gr%np)*kpt%nlocal)
      call accel_write_buffer(phase%buff_phase_corr, (gr%np_part - gr%np)*kpt%nlocal, phase%phase_corr)
    end if

    POP_SUB(phase_init_phases)
  end subroutine phase_init_phases

  ! ----------------------------------------------------------------------------------
  !> @brief Update the phases
  subroutine phase_update_phases(phase, mesh, kpt, kpoints, d, space, uniform_vector_potential)
    class(phase_t),      intent(inout) :: phase
    class(mesh_t),       intent(in)    :: mesh
    type(distributed_t), intent(in)    :: kpt
    type(kpoints_t),     intent(in)    :: kpoints
    type(states_elec_dim_t), intent(in)  :: d
    type(space_t),       intent(in)    :: space
    FLOAT, allocatable,  intent(in)    :: uniform_vector_potential(:)

    integer :: ik, ip, sp
    integer(int64) :: ip_inner_global
    FLOAT   :: kpoint(space%dim)
    FLOAT, allocatable :: x_global(:,:)

    PUSH_SUB(phase_update_phases)

    if (allocated(uniform_vector_potential)) then

      if (.not. allocated(phase%phase)) then
        SAFE_ALLOCATE(phase%phase(1:mesh%np_part, kpt%start:kpt%end))
        if (accel_is_enabled()) then
          call accel_create_buffer(phase%buff_phase, ACCEL_MEM_READ_ONLY, TYPE_CMPLX, &
            mesh%np_part*kpt%nlocal)
        end if
      end if

      if (.not. allocated(phase%phase_corr)) then
        SAFE_ALLOCATE(phase%phase_corr(mesh%np+1:mesh%np_part, kpt%start:kpt%end))
        if (accel_is_enabled()) then
          call accel_create_buffer(phase%buff_phase_corr, ACCEL_MEM_READ_ONLY, TYPE_CMPLX, &
            (mesh%np_part - mesh%np)*kpt%nlocal)
        end if
      end if

      kpoint(1:space%dim) = M_ZERO
      ! loop over boundary points
      sp = mesh%np
      ! skip ghost points
      if (mesh%parallel_in_domains) sp = mesh%np + mesh%pv%np_ghost

      SAFE_ALLOCATE(x_global(1:space%dim,(sp + 1):mesh%np_part))
      !$omp parallel private(ik, ip, kpoint, ip_inner_global)
      !$omp do schedule(static)
      do ip = sp + 1, mesh%np_part
        ! get corresponding inner point
        ip_inner_global = mesh_periodic_point(mesh, space, ip)

        ! compute phase correction from global coordinate (opposite sign!)
        x_global(:,ip) = mesh_x_global(mesh, ip_inner_global)
      end do

      do ik = kpt%start, kpt%end
        kpoint(1:space%dim) = kpoints%get_point(d%get_kpoint_index(ik))
        !We add the vector potential
        kpoint(1:space%dim) = kpoint(1:space%dim) + uniform_vector_potential(1:space%dim)

        !$omp do simd schedule(static)
        do ip = 1, mesh%np_part
          phase%phase(ip, ik) = exp(-M_zI*sum(mesh%x(ip, 1:space%dim)*kpoint(1:space%dim)))
        end do
        !$omp end do simd

        !$omp do schedule(static)
        do ip = sp + 1, mesh%np_part
          phase%phase_corr(ip, ik) = M_zI * sum(x_global(1:space%dim, ip) * kpoint(1:space%dim))
          phase%phase_corr(ip, ik) = exp(phase%phase_corr(ip, ik))*phase%phase(ip, ik)
        end do
        !$omp end do nowait
      end do
      !$omp end parallel
      SAFE_DEALLOCATE_A(x_global)

      if (accel_is_enabled()) then
        call accel_write_buffer(phase%buff_phase, mesh%np_part*kpt%nlocal, phase%phase, async=.true.)
        call accel_write_buffer(phase%buff_phase_corr, (mesh%np_part - mesh%np)*kpt%nlocal, &
          phase%phase_corr, async=.true.)
      end if
    end if

    POP_SUB(phase_update_phases)
  end subroutine phase_update_phases

  ! ----------------------------------------------------------------------------------
  !> @brief Releases the memory of the phase object
  subroutine phase_end(phase)
    class(phase_t),  intent(inout) :: phase

    PUSH_SUB(phase_end)

    if (phase%is_allocated() .and. accel_is_enabled()) then
      call accel_release_buffer(phase%buff_phase)
      call accel_release_buffer(phase%buff_phase_corr)
    end if

    if (allocated(phase%phase_spiral) .and. accel_is_enabled()) then
      call accel_release_buffer(phase%buff_phase_spiral)
    end if

    SAFE_DEALLOCATE_A(phase%phase)
    SAFE_DEALLOCATE_A(phase%phase_corr)
    SAFE_DEALLOCATE_A(phase%phase_spiral)

    POP_SUB(phase_end)
  end subroutine phase_end

  ! ----------------------------------------------------------------------------------
  !> @brief set the phase correction (if necessary)
  !
  subroutine phase_set_phase_corr(phase, mesh, psib, async)
    class(phase_t),                intent(in) :: phase
    class(mesh_t),                 intent(in) :: mesh
    type(wfs_elec_t),           intent(inout) :: psib
    logical, optional,             intent(in) :: async


    logical :: phase_correction

    PUSH_SUB(phase_set_phase_corr)

    ! check if we only want a phase correction for the boundary points
    phase_correction = phase%is_allocated()

    !We apply the phase only to np points, and the phase for the np+1 to np_part points
    !will be treated as a phase correction in the Hamiltonian
    if (phase_correction) then
      call phase%apply_to(mesh, mesh%np, .false., psib, async=async)
    end if

    POP_SUB(phase_set_phase_corr)
  end subroutine phase_set_phase_corr

  ! ----------------------------------------------------------------------------------
  !> @brief unset the phase correction (if necessary)
  !
  subroutine phase_unset_phase_corr(phase, mesh, psib, async)
    class(phase_t),                intent(in) :: phase
    class(mesh_t),                 intent(in) :: mesh
    type(wfs_elec_t),           intent(inout) :: psib
    logical, optional,             intent(in) :: async

    logical :: phase_correction

    PUSH_SUB(phase_unset_phase_corr)

    ! check if we only want a phase correction for the boundary points
    phase_correction = phase%is_allocated()

    !We apply the phase only to np points, and the phase for the np+1 to np_part points
    !will be treated as a phase correction in the Hamiltonian
    if (phase_correction) then
      call phase%apply_to(mesh, mesh%np, .true., psib, async=async)
    end if

    POP_SUB(phase_unset_phase_corr)
  end subroutine phase_unset_phase_corr

  ! ---------------------------------------------------------------------------------------
  !> @brief apply (remove) the phase to the wave functions before (after) applying the Hamiltonian
  !
  subroutine phase_apply_batch(this, mesh, np, conjugate, psib, src, async)
    class(phase_t),                        intent(in)    :: this
    class(mesh_t),                         intent(in)    :: mesh
    integer,                               intent(in)    :: np
    logical,                               intent(in)    :: conjugate
    type(wfs_elec_t),              target, intent(inout) :: psib
    type(wfs_elec_t),    optional, target, intent(in)    :: src
    logical, optional,                     intent(in)    :: async

    integer :: ip, ii, sp
    type(wfs_elec_t), pointer :: src_
    type(profile_t), save :: phase_prof
    CMPLX :: phase
    integer(int64) :: wgsize, dim2, dim3
    type(accel_kernel_t), save :: ker_phase

    PUSH_SUB(phase_apply_batch)
    call profiling_in(phase_prof, "PHASE_APPLY_BATCH")

    call profiling_count_operations(6*np*psib%nst_linear)

    ASSERT(np <= mesh%np_part)
    ASSERT(psib%type() == TYPE_CMPLX)

    src_ => psib
    if (present(src)) src_ => src

    ASSERT(src_%has_phase .eqv. conjugate)
    ASSERT(src_%ik == psib%ik)
    ASSERT(src_%type() == TYPE_CMPLX)

    ! We want to skip the ghost points for setting the phase
    sp = min(np, mesh%np)
    if (np > mesh%np .and. mesh%parallel_in_domains) sp = mesh%np + mesh%pv%np_ghost

    select case (psib%status())
    case (BATCH_PACKED)

      if (conjugate) then

        !$omp parallel private(ii, phase)
        !$omp do
        do ip = 1, min(mesh%np, np)
          phase = conjg(this%phase(ip, psib%ik))
          !$omp simd
          do ii = 1, psib%nst_linear
            psib%zff_pack(ii, ip) = phase*src_%zff_pack(ii, ip)
          end do
        end do
        !$omp end do nowait

        ! Boundary points, if requested
        !$omp do
        do ip = sp+1, np
          phase = conjg(this%phase(ip, psib%ik))
          !$omp simd
          do ii = 1, psib%nst_linear
            psib%zff_pack(ii, ip) = phase*src_%zff_pack(ii, ip)
          end do
        end do
        !$omp end parallel

      else

        !$omp parallel private(ii, phase)
        !$omp do
        do ip = 1, min(mesh%np, np)
          phase = this%phase(ip, psib%ik)
          !$omp simd
          do ii = 1, psib%nst_linear
            psib%zff_pack(ii, ip) = phase*src_%zff_pack(ii, ip)
          end do
        end do
        !$omp end do nowait

        ! Boundary points, if requested
        !$omp do
        do ip = sp+1, np
          phase = this%phase(ip, psib%ik)
          !$omp simd
          do ii = 1, psib%nst_linear
            psib%zff_pack(ii, ip) = phase*src_%zff_pack(ii, ip)
          end do
        end do
        !$omp end parallel

      end if

    case (BATCH_NOT_PACKED)

      if (conjugate) then

        !$omp parallel private(ii, ip)
        do ii = 1, psib%nst_linear
          !$omp do simd
          do ip = 1, min(mesh%np, np)
            psib%zff_linear(ip, ii) = conjg(this%phase(ip, psib%ik))*src_%zff_linear(ip, ii)
          end do
          !$omp end do simd nowait

          ! Boundary points, if requested
          !$omp do simd
          do ip = sp+1, np
            psib%zff_linear(ip, ii) = conjg(this%phase(ip, psib%ik))*src_%zff_linear(ip, ii)
          end do
          !$omp end do simd nowait
        end do
        !$omp end parallel

      else
        !$omp parallel private(ii, ip)
        do ii = 1, psib%nst_linear
          !$omp do simd
          do ip = 1, min(mesh%np, np)
            psib%zff_linear(ip, ii) = this%phase(ip, psib%ik)*src_%zff_linear(ip, ii)
          end do
          !$omp end do simd nowait

          ! Boundary points, if requested
          !$omp do simd
          do ip = sp+1, np
            psib%zff_linear(ip, ii) = this%phase(ip, psib%ik)*src_%zff_linear(ip, ii)
          end do
          !$omp end do simd nowait

        end do
        !$omp end parallel

      end if

    case (BATCH_DEVICE_PACKED)
      call accel_kernel_start_call(ker_phase, 'phase.cl', 'phase_hamiltonian')

      if (conjugate) then
        call accel_set_kernel_arg(ker_phase, 0, 1_4)
      else
        call accel_set_kernel_arg(ker_phase, 0, 0_4)
      end if

      call accel_set_kernel_arg(ker_phase, 1, (psib%ik - this%buff_phase_qn_start)*mesh%np_part)
      call accel_set_kernel_arg(ker_phase, 2, np)
      call accel_set_kernel_arg(ker_phase, 3, this%buff_phase)
      call accel_set_kernel_arg(ker_phase, 4, src_%ff_device)
      call accel_set_kernel_arg(ker_phase, 5, log2(int(src_%pack_size(1), int32)))
      call accel_set_kernel_arg(ker_phase, 6, psib%ff_device)
      call accel_set_kernel_arg(ker_phase, 7, log2(int(psib%pack_size(1), int32)))

      wgsize = accel_kernel_workgroup_size(ker_phase)/psib%pack_size(1)

      dim3 = np/(accel_max_size_per_dim(2)*wgsize) + 1
      dim2 = min(accel_max_size_per_dim(2)*wgsize, pad(np, wgsize))

      call accel_kernel_run(ker_phase, (/psib%pack_size(1), dim2, dim3/), (/psib%pack_size(1), wgsize, 1_int64/))

      if(.not. optional_default(async, .false.)) call accel_finish()
    end select

    psib%has_phase = .not. conjugate

    call profiling_out(phase_prof)
    POP_SUB(phase_apply_batch)
  end subroutine phase_apply_batch

  !> @brief apply (or remove) the phase to a wave function psi
  !!
  !! States are usually stored _without_ the phase. Due to the phase convention (exp(-i phase())),
  !! the phase is applied by multiplying with the complex conjugate of the phase() function,
  !! and removed by multiplying with phase().
  !
  subroutine phase_apply_mf(this, psi, np, dim, ik, conjugate)
    class(phase_t),         intent(in)    :: this
    CMPLX,               intent(inout)    :: psi(:, :)  !< the complex wave function
    integer,                intent(in)    :: np         !< number of points
    integer,                intent(in)    :: dim
    integer,                intent(in)    :: ik
    logical,                intent(in)    :: conjugate  !< if .false.: apply the phase, if .true.: remove the phase

    integer :: idim, ip
    type(profile_t), save :: prof

    PUSH_SUB(phase_apply_mf)

    call profiling_in(prof, "PHASE_APPLY_SINGLE")

    if (conjugate) then
      ! Apply the phase that contains both the k-point and vector-potential terms.
      do idim = 1, dim
        !$omp parallel do
        do ip = 1, np
          psi(ip, idim) = conjg(this%phase(ip, ik))*psi(ip, idim)
        end do
        !$omp end parallel do
      end do
    else
      ! Apply the conjugate of (i.e. remove) the phase that contains both the k-point and vector-potential terms.
      do idim = 1, dim
        !$omp parallel do
        do ip = 1, np
          psi(ip, idim) = this%phase(ip, ik)*psi(ip, idim)
        end do
        !$omp end parallel do
      end do
    end if

    call profiling_out(prof)

    POP_SUB(phase_apply_mf)
  end subroutine phase_apply_mf


  ! ---------------------------------------------------------------------------------------
  !> @brief apply spiral phase
  !
  subroutine phase_phase_spiral(this, der, psib)
    class(phase_t),         intent(in)    :: this
    type(derivatives_t),    intent(in)    :: der
    class(wfs_elec_t),      intent(inout) :: psib

    integer               :: ip, ii, sp
    integer, allocatable  :: spin_label(:)
    type(accel_mem_t)     :: spin_label_buffer
    type(profile_t), save :: phase_prof
    integer(int64) :: wgsize

    PUSH_SUB(phase_phase_spiral)
    call profiling_in(phase_prof, "PBC_PHASE_SPIRAL")

    call profiling_count_operations(6*(der%mesh%np_part-der%mesh%np)*psib%nst_linear)

    ASSERT(der%boundaries%spiral)
    ASSERT(psib%type() == TYPE_CMPLX)

    sp = der%mesh%np
    if (der%mesh%parallel_in_domains) sp = der%mesh%np + der%mesh%pv%np_ghost


    select case (psib%status())
    case (BATCH_PACKED)

      !$omp parallel do private(ip, ii)
      do ip = sp + 1, der%mesh%np_part
        do ii = 1, psib%nst_linear, 2
          if (this%spin(3,psib%linear_to_ist(ii), psib%ik)>0) then
            psib%zff_pack(ii+1, ip) = psib%zff_pack(ii+1, ip)*this%phase_spiral(ip-sp, 1)
          else
            psib%zff_pack(ii, ip) = psib%zff_pack(ii, ip)*this%phase_spiral(ip-sp, 2)
          end if
        end do
      end do
      !$omp end parallel do

    case (BATCH_NOT_PACKED)

      !$omp parallel private(ii, ip)
      do ii = 1, psib%nst_linear, 2
        if (this%spin(3,psib%linear_to_ist(ii), psib%ik)>0) then
          !$omp do
          do ip = sp + 1, der%mesh%np_part
            psib%zff_linear(ip, ii+1) = psib%zff_linear(ip, ii+1)*this%phase_spiral(ip-sp, 1)
          end do
          !$omp end do nowait
        else
          !$omp do
          do ip = sp + 1, der%mesh%np_part
            psib%zff_linear(ip, ii) = psib%zff_linear(ip, ii)*this%phase_spiral(ip-sp, 2)
          end do
          !$omp end do nowait
        end if
      end do
      !$omp end parallel

    case (BATCH_DEVICE_PACKED)

      ASSERT(accel_is_enabled())

      ! generate array of offsets for access of psib and phase_spiral:
      ! TODO: Move this to the routine where spin(:,:,:) is generated
      !       and also move the buffer to the GPU at this point to
      !       avoid unecessary latency here!

      SAFE_ALLOCATE(spin_label(1:psib%nst_linear))
      spin_label = 0
      do ii = 1, psib%nst_linear, 2
        if (this%spin(3, psib%linear_to_ist(ii), psib%ik) > 0) spin_label(ii)=1
      end do

      call accel_create_buffer(spin_label_buffer, ACCEL_MEM_READ_ONLY, TYPE_INTEGER, psib%nst_linear)
      call accel_write_buffer(spin_label_buffer, psib%nst_linear, spin_label)

      call accel_kernel_start_call(kernel_phase_spiral, 'phase_spiral.cl', 'phase_spiral_apply')

      call accel_set_kernel_arg(kernel_phase_spiral, 0, psib%nst)
      call accel_set_kernel_arg(kernel_phase_spiral, 1, sp)
      call accel_set_kernel_arg(kernel_phase_spiral, 2, der%mesh%np_part)
      call accel_set_kernel_arg(kernel_phase_spiral, 3, psib%ff_device)
      call accel_set_kernel_arg(kernel_phase_spiral, 4, log2(psib%pack_size(1)))
      call accel_set_kernel_arg(kernel_phase_spiral, 5, this%buff_phase_spiral)
      call accel_set_kernel_arg(kernel_phase_spiral, 6, spin_label_buffer)

      wgsize = accel_kernel_workgroup_size(kernel_phase_spiral)/psib%pack_size(1)

      call accel_kernel_run(kernel_phase_spiral, &
        (/psib%pack_size(1)/2, pad(der%mesh%np_part - sp, 2*wgsize)/), &
        (/psib%pack_size(1)/2, 2*wgsize/))

      call accel_finish()

      call accel_release_buffer(spin_label_buffer)

      SAFE_DEALLOCATE_A(spin_label)

    end select

    call profiling_out(phase_prof)
    POP_SUB(phase_phase_spiral)
  end subroutine phase_phase_spiral


  ! ---------------------------------------------------------------------------------------
  logical pure function phase_is_allocated(this)
    class(phase_t), intent(in) :: this

    phase_is_allocated = allocated(this%phase)
  end function phase_is_allocated

end module phase_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
