!! Copyright (C) 2023  L. Konecny,  M. Lueders
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module implements the ZORA terms for the Hamoiltonian

module zora_oct_m

  use batch_oct_m
  use batch_ops_oct_m
  use debug_oct_m
  use derivatives_oct_m
  use epot_oct_m
  use global_oct_m
  use grid_oct_m
  use hamiltonian_elec_base_oct_m
  use mesh_oct_m
  use messages_oct_m
  use namespace_oct_m
  use profiling_oct_m
  use states_elec_dim_oct_m
  use wfs_elec_oct_m

  implicit none

  private
  public :: zora_t

  integer, parameter, public :: &
    ZORA_NONE                = 0,     &  !< no ZORA
    ZORA_SCALAR_RELATIVISTIC = 1,     &  !< ZORA for scalar relativistic calculations
    ZORA_FULLY_RELATIVISTIC  = 2         !< ZORA for fully relativistic calculations

  !> @brief This class is responsible for calculating and applying the ZORA
  !
  type :: zora_t
    private

    FLOAT, allocatable  :: pot(:,:)          !< ZORA mass renormalization term, \f$ \frac{c^2}{2c^2 - V} \f$
    FLOAT, allocatable  :: grad_pot(:,:,:)   !< gradient of ZORA mass renormalization term, \f$ \nabla ( \frac{c^2}{2c^2 - V} ) \f$
    FLOAT, allocatable  :: soc(:,:,:)        !< ZORA spin-orbit term, \f$ {\rm zora\%pref} * (\sigma \times {\nabla V}) \cdot p \f$

    FLOAT   :: so_strength = M_ZERO
    FLOAT   :: mass        = M_ZERO

    integer :: zora_level  = ZORA_NONE

  contains
    procedure :: update => zora_update              !< @copydoc zora_update()
    procedure :: dapply_batch => dzora_apply_batch  !< @copydoc dzora_apply_batch()
    procedure :: zapply_batch => zzora_apply_batch  !< @copydoc zzora_apply_batch()
    final     :: zora_finalize                      !< @copydoc zora_finalize()
  end type

  interface zora_t
    procedure zora_constructor
  end interface zora_t

contains

  !> @brief initialize the ZORA
  !!
  !! allocate memory for ZORA potentials.
  !
  function zora_constructor(namespace, der, st_d, ep, mass) result(this)
    type(namespace_t),           intent(in)    :: namespace
    type(derivatives_t),         intent(in)    :: der
    type(states_elec_dim_t),     intent(in)    :: st_d
    type(epot_t),                intent(in)    :: ep
    FLOAT,                       intent(in)    :: mass
    class(zora_t), pointer                     :: this

    PUSH_SUB(zora_constructor)
    SAFE_ALLOCATE(this)


    this%so_strength   = ep%so_strength
    this%mass          = mass

    select case (ep%reltype)
    case(SCALAR_RELATIVISTIC_ZORA)
      this%zora_level = ZORA_SCALAR_RELATIVISTIC
    case(FULLY_RELATIVISTIC_ZORA)
      this%zora_level = ZORA_FULLY_RELATIVISTIC
    case default
      this%zora_level = ZORA_NONE
      POP_SUB(zora_constructor)
      return
    end select

    if (der%dim /= 3) then
      call messages_not_implemented("ZORA for dimensions /= 3", namespace)
    end if

    SAFE_ALLOCATE(this%pot(1:der%mesh%np_part, 1:st_d%nspin))
    SAFE_ALLOCATE(this%grad_pot(1:der%mesh%np, 1:st_d%nspin, 1:der%dim))

    if(this%zora_level == ZORA_FULLY_RELATIVISTIC) then

      ASSERT(st_d%nspin == 4)
      SAFE_ALLOCATE(this%soc(1:der%mesh%np, 1:st_d%nspin, 1:der%dim))

    end if

    POP_SUB(zora_constructor)
  end function zora_constructor


  !> @brief finalize the ZORA object and free memory
  !
  subroutine zora_finalize(this)
    type(zora_t), intent(inout) :: this

    PUSH_SUB(zora_finalize)

    SAFE_DEALLOCATE_A(this%pot)
    SAFE_DEALLOCATE_A(this%grad_pot)
    SAFE_DEALLOCATE_A(this%soc)

    POP_SUB(zora_finalize)
  end subroutine zora_finalize


  !> @brief update the ZORA potentials
  !!
  !! This routine performs calculations, which do not require the wave functions
  !! but only depend on the potential.
  !!
  !! It calculates:
  !!
  !! - \f$ {\rm zora\%pot}({\bf r})   =  \frac{c^2}{2c^2 - V({\bf r})} \f$
  !! - \f$ {\rm zora\%grad_pot}({\bf r})  =  \nabla \frac{c^2}{2c^2 - V({\bf r})} \f$
  !!
  !! and for the fully relativistic case
  !! - \f$ {\rm zora\%soc}({\bf r})  =  {\rm prefactor}({\bf r}) \,  \sigma \cdot ( \nabla V({\bf r}) \times {\bf p} )\f$
  !!              = \f$ {\rm prefactor}({\bf r}) * (\sigma \times ( \nabla V)({\bf r})) \cdot {\bf p} \f$
  !!
  !!   where
  !!   \f$ {\rm prefactor}({\bf r}) = {\rm so\_strength} *  2 c^2 / (2 m c^2 - {\rm V({\bf r})^2} ) \f$
  !!
  !! These are used in dzora_apply_batch() and zzora_apply_batch().
  !
  subroutine zora_update(this, der, potential)
    class(zora_t),         intent(inout) :: this            !< the ZORA object
    class (derivatives_t), intent(in)    :: der             !< the derivatives
    FLOAT, contiguous,     intent(in)    :: potential(:, :) !< dimensions (1:np, 1:this%spin_channels)

    FLOAT, allocatable :: zora_scpot(:), grad_pot(:,:)
    FLOAT              :: prefactor, two_c2

    integer :: ip, idir
    integer :: nspin

    PUSH_SUB(zora_update)

    if (this%zora_level == ZORA_NONE) then
      POP_SUB(zora_update)
      return
    end if

    nspin = size(this%pot, dim=2)
    two_c2 = M_TWO * P_C**2

    ! We need to copy the potential to an array, allocated to np_part for the application of the derivatives.

    SAFE_ALLOCATE( zora_scpot(1:der%mesh%np_part) )
    SAFE_ALLOCATE( grad_pot(1:der%mesh%np_part, 1:der%dim) )

    !$omp parallel private(ip)
    !$omp do simd schedule(static)
    do ip = 1, der%mesh%np
      ! zora_scpot = V
      zora_scpot(ip) = potential(ip, 1)
      ! zora_pot   = \frac{c^2}{2 m c^2 - V}
      ! included extra factor of 2 because of -1/2 in kinetic energy
      this%pot(ip,1) = two_c2 / (this%mass * two_c2 - zora_scpot(ip) )
    end do
    !$omp end do simd

    if (nspin > 1) then
      !$omp do simd schedule(static)
      do ip = 1, der%mesh%np
        this%pot(ip,2) = this%pot(ip,1)
      end do
      !$omp end do simd
    end if
    if (nspin > 2) then
      !$omp do simd schedule(static)
      do ip = 1, der%mesh%np
        this%pot(ip,3) = M_ZERO
        this%pot(ip,4) = M_ZERO
      end do
      !$omp end do simd
    end if
    !$omp end parallel

    ! zora_grad_pot   = grad( \frac{c^2}{2 m c^2 - V} )

    call dderivatives_grad(der, this%pot(:,1), grad_pot)

    do idir=1, der%dim
      !$omp parallel private(ip)
      !$omp do simd schedule(static)
      do ip = 1, der%mesh%np
        this%grad_pot(ip, 1, idir)= grad_pot(ip,idir)
      end do
      !$omp end do simd

      if (nspin > 1) then
        !$omp do simd schedule(static)
        do ip = 1, der%mesh%np
          this%grad_pot(ip, 2, idir)= grad_pot(ip,idir)
        end do
        !$omp end do simd
      end if

      if (nspin > 2) then
        !$omp do simd schedule(static)
        do ip = 1, der%mesh%np
          this%grad_pot(ip, 3, idir)= M_ZERO
          this%grad_pot(ip, 4, idir)= M_ZERO
        end do
        !$omp end do simd
      end if
      !$omp end parallel

    end do

    ! spin-orbit relativistic ZORA contribution
    if (this%zora_level == ZORA_FULLY_RELATIVISTIC) then
      call zora_update_fully_relativistic()
    end if

    SAFE_DEALLOCATE_A( zora_scpot )
    SAFE_DEALLOCATE_A( grad_pot )

    POP_SUB(zora_update)

  contains

    !> @brief update quantities, necessary only for fully relativistic ZORA
    !!
    !! soc(:,:,1) = \f$ (\boldsymbol{\sigma} \times \nabla V )_x = (-(\nabla V)_y, (\nabla V)_y, 0, -(\nabla V)_z) \f$
    !! soc(:,:,2) = \f$ (\boldsymbol{\sigma} \times \nabla V )_y = ((\nabla V)_x,  -(\nabla V)_x, -(\nabla V)_z, 0) \f$
    !! soc(:,:,3) = \f$ (\boldsymbol{\sigma} \times \nabla V )_z = (0, 0, (\nabla V)_y, (\nabla V)_x) \f$

    subroutine zora_update_fully_relativistic()

      FLOAT, allocatable :: grad_v(:,:)

      PUSH_SUB(zora_update_fully_relativistic)

      SAFE_ALLOCATE(grad_v(1:der%mesh%np, 1:der%dim))


      ! gradient of potenial, only consider scalar potential
      call dderivatives_grad(der, zora_scpot(:), grad_v(:,:))

      !$omp parallel private(ip, prefactor)
      !$omp do simd schedule(static)
      do ip = 1, der%mesh%np

        ! added extra factor of 2 because of -1/2 in the gradient
        prefactor = this%so_strength *  two_c2 / (this%mass*two_c2 - zora_scpot(ip))**2

        grad_v(ip, 1) = grad_v(ip, 1) * prefactor
        grad_v(ip, 2) = grad_v(ip, 2) * prefactor
        grad_v(ip, 3) = grad_v(ip, 3) * prefactor

        ! sigma \times zora_grad_v
        ! four-component vector has elements ( up-up, down-down, Re(up-down), Im(up-down) )
        this%soc(ip, 1, 1) = -grad_v(ip, 2)
        this%soc(ip, 2, 1) =  grad_v(ip, 2)
        this%soc(ip, 3, 1) =  M_ZERO
        this%soc(ip, 4, 1) = -grad_v(ip, 3)

        this%soc(ip, 1, 2) =  grad_v(ip, 1)
        this%soc(ip, 2, 2) = -grad_v(ip, 1)
        this%soc(ip, 3, 2) = -grad_v(ip, 3)
        this%soc(ip, 4, 2) =  M_ZERO

        this%soc(ip, 1, 3) =  M_ZERO
        this%soc(ip, 2, 3) =  M_ZERO
        this%soc(ip, 3, 3) =  grad_v(ip, 2)
        this%soc(ip, 4, 3) =  grad_v(ip, 1)

      end do
      !$omp end do simd
      !$omp end parallel

      SAFE_DEALLOCATE_A(grad_v)

      POP_SUB(zora_update_fully_relativistic)
    end subroutine zora_update_fully_relativistic

  end subroutine zora_update

#include "undef.F90"
#include "real.F90"
#include "zora_inc.F90"

#include "undef.F90"
#include "complex.F90"
#include "zora_inc.F90"


end module zora_oct_m
