target_sources(Octopus_lib PRIVATE
		chebyshev_filter_bounds.F90
		chebyshev_filter_oracle.F90
		current.F90
		dos.F90
		eigen_cg.F90
		eigen_chebyshev.F90
		eigen_rmmdiis.F90
		eigensolver.F90
		electrons.F90
		electron_space.F90
		elf.F90
		energy_calc.F90
		exponential.F90
		forces.F90
		kpoints.F90
		linear_solver.F90
		magnetic.F90
		matter.F90
		partial_charges.F90
		perturbation.F90
		perturbation_electric.F90
		perturbation_ionic.F90
		perturbation_kdotp.F90
		perturbation_magnetic.F90
		perturbation_none.F90
		preconditioners.F90
		scdm.F90
		stress.F90
		subspace.F90
		x_fbe.F90
		x_slater.F90
		v_ks.F90
		xc_ks_inversion.F90
		xc_oep.F90
		xc_photons.F90
		xc_sic.F90)

## External libraries
target_link_libraries(Octopus_lib PRIVATE dftd3)
