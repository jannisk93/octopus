!! Copyright (C) 2020 M. Oliveira
!!
!! This Source Code Form is subject to the terms of the Mozilla Public
!! License, v. 2.0. If a copy of the MPL was not distributed with this
!! file, You can obtain one at https://mozilla.org/MPL/2.0/.
!!

#include "global.h"

module ghost_interaction_oct_m
  use debug_oct_m
  use global_oct_m
  use interaction_oct_m
  use interaction_partner_oct_m
  use profiling_oct_m

  implicit none

  private
  public ::                &
    ghost_interaction_t

  !> The ghost ineraction is a dummy interaction, which needs to be setup between otherwise non-interacting
  !! interaction partners, to ensure synchronicity between the systems.
  type, extends(interaction_t) :: ghost_interaction_t
  contains
    procedure :: calculate => ghost_interaction_calculate
    procedure :: calculate_energy => ghost_interaction_calculate_energy
    final :: ghost_interaction_finalize
  end type ghost_interaction_t

  interface ghost_interaction_t
    module procedure ghost_interaction_init
  end interface ghost_interaction_t

contains

  ! ---------------------------------------------------------
  function ghost_interaction_init(partner) result(this)
    class(interaction_partner_t), target, intent(inout) :: partner
    class(ghost_interaction_t),           pointer       :: this

    PUSH_SUB(ghost_interaction_init)

    allocate(this)

    this%label = "ghost"

    this%partner => partner
    this%intra_interaction = .false.

    POP_SUB(ghost_interaction_init)
  end function ghost_interaction_init

  ! ---------------------------------------------------------
  subroutine ghost_interaction_calculate(this)
    class(ghost_interaction_t), intent(inout) :: this

    PUSH_SUB(ghost_interaction_calculate)

    ! A ghost interaction does not do anything

    POP_SUB(ghost_interaction_calculate)
  end subroutine ghost_interaction_calculate

  ! ---------------------------------------------------------
  subroutine ghost_interaction_calculate_energy(this)
    class(ghost_interaction_t), intent(inout) :: this

    PUSH_SUB(ghost_interaction_calculate_energy)

    ! A ghost interaction does not have an energy

    this%energy = M_ZERO

    POP_SUB(ghost_interaction_calculate_energy)
  end subroutine ghost_interaction_calculate_energy


  ! ---------------------------------------------------------
  subroutine ghost_interaction_finalize(this)
    type(ghost_interaction_t), intent(inout) :: this

    PUSH_SUB(ghost_interaction_finalize)

    call interaction_end(this)

    POP_SUB(ghost_interaction_finalize)
  end subroutine ghost_interaction_finalize

end module ghost_interaction_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
