target_sources(Octopus_lib PRIVATE
		# #643 TODO: Separate as different library
		../../liboct_parser/gsl_userdef.c
		../../liboct_parser/parse.c
		../../liboct_parser/parse_exp.c
		../../liboct_parser/parser_f.c
		../../liboct_parser/symbols.c
		accel.F90
		alloc_cache.F90
		alloc_cache_low.cc
		blacs.F90
		blacs_proc_grid.F90
		calc_mode_par.F90
		cgal_polyhedra.F90
		clblas.F90
		clblas_low.c
		clblast_low.c
		comm.F90
		cublas.cc
		cuda.F90
		cuda_low.cc
		debug.F90
		distributed.F90
		gdlib.F90
		gdlib_f.c
		getopt_f.c
		global.F90
		hardware.F90
		heap.F90
		iihash.F90
		iihash_low.cc
		io.F90
		io_binary.c
		io_binary_f.F90
		io_csv.c
		io_csv_f.F90
		lattice_vectors.F90
		linked_list.F90
		list_node.F90
		loct.F90
		lookup.F90
		merge_sorted.F90
		messages.F90
		multicomm.F90
		namespace.F90
		nvtx.F90
		nvtx_low.cc
		oct_f.c
		parser.F90
		profiling.F90
		recipes.c
		ring_pattern.F90
		signals.c
		sihash.F90
		sihash_low.cc
		sort.F90
		sort_low.cc
		space.F90
		sphash.F90
		sphash_low.cc
		string.F90
		types.F90
		unit.F90
		unit_system.F90
		utils.F90
		varia.c
		varinfo.F90
		varinfo_low.c
		walltimer.F90
		write_iter.F90
		write_iter_low.cc
		mpi.F90
		mpi_debug.F90
		mpi_lib.F90
		mpi_test.F90
		)
target_sources(fortran_cli PRIVATE
		command_line.F90)

## Link targets
target_link_libraries(fortran_cli PRIVATE Octopus_base)
target_include_directories(Octopus_lib PRIVATE ../../liboct_parser)

## External libraries
target_link_libraries(Octopus_lib PRIVATE GSL::gsl)
if (TARGET OpenMP::OpenMP_Fortran)
	target_link_libraries(Octopus_lib PRIVATE OpenMP::OpenMP_Fortran)
	target_compile_definitions(Octopus_lib PRIVATE HAVE_OPENMP)
endif ()
if (TARGET MPI::MPI_Fortran)
	target_link_libraries(Octopus_lib PUBLIC MPI::MPI_Fortran)
	if (CMAKE_Fortran_COMPILER_ID STREQUAL GNU)
		target_compile_options(Octopus_lib PRIVATE $<$<COMPILE_LANGUAGE:Fortran>:-fallow-argument-mismatch>)
	endif ()
endif ()

if (TARGET OpenCL::OpenCL)
	target_link_libraries(Octopus_lib PRIVATE OpenCL::OpenCL clblast clFFT fortrancl)
endif ()
if (OCTOPUS_CUDA)
	target_link_libraries(Octopus_lib PRIVATE
		CUDA::cudart
		CUDA::cuda_driver
		CUDA::cublas
		CUDA::cufft
		CUDA::nvrtc
		CUDA::nvToolsExt)
endif ()

if (TARGET netCDF::Fortran)
	target_link_libraries(Octopus_lib PRIVATE netCDF::Fortran)
endif ()
if (TARGET CGAL::CGAL)
	target_link_libraries(Octopus_lib PRIVATE CGAL::CGAL)
	target_sources(Octopus_lib PRIVATE
			cgal_polyhedra_low.cc
	)
endif ()
if (TARGET GD::GD)
	target_link_libraries(Octopus_lib PRIVATE GD::GD)
endif ()
